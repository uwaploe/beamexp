// Package beamexp provides an interface to a Sill Optics S6MEC3914 Beam
// Expander controller.
package beamexp

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"strings"
)

const (
	// Command terminator
	EOT = "\r"
	// Response terminator
	EOL = "\r\n"
	// Controller prompt (note trailing space)
	PROMPT = "-> "
)

// Limits of allowable offset values
const MaxOffset int = 3200
const MinOffset int = 0

var ErrOffset = errors.New("Offset out of range")

// respScanner parses the command responses from the device
type respScanner struct {
	rdr     io.Reader
	respBuf bytes.Buffer
	prompt  []byte
	tail    int
}

func newScanner(rdr io.Reader, prompt string) *respScanner {
	return &respScanner{
		rdr:    rdr,
		prompt: []byte(prompt),
	}
}

func (s *respScanner) next() (string, error) {
	s.respBuf.Reset()
	s.tail = -len(s.prompt)
	b := make([]byte, 1)
	for {
		_, err := s.rdr.Read(b)
		if err != nil {
			return s.respBuf.String(), err
		}

		s.respBuf.Write(b)
		s.tail++
		if s.tail < 0 {
			continue
		}

		if bytes.Equal(s.respBuf.Bytes()[s.tail:], s.prompt) {
			return strings.TrimSuffix(
				strings.TrimLeft(s.respBuf.String(), "\x00\xff"),
				string(s.prompt)), nil
		}

	}

	return "", nil
}

type Device struct {
	port    io.ReadWriter
	scanner *respScanner
}

// New returns a new *Device instance attached to an io.ReadWriter.
func New(rw io.ReadWriter) *Device {
	return &Device{
		port:    rw,
		scanner: newScanner(rw, PROMPT),
	}
}

// Exec sends a command to the Device and returns the response.
func (d *Device) Exec(cmd string) (string, error) {
	var err error

	switch cmd {
	case "+", "-":
		_, err = d.port.Write([]byte(cmd))
	default:
		_, err = d.port.Write([]byte(cmd + EOL))
	}

	if err != nil {
		return "", err
	}

	resp, err := d.scanner.next()
	if err != nil {
		return resp, err
	}

	return strings.TrimLeft(resp, "<- "), nil
}

// Offset returns the current offset value
func (d *Device) Offset() (int, error) {
	var offset int
	resp, err := d.Exec("?f")
	if err != nil {
		return offset, err
	}
	// Response string = "Offset: NNNN"
	_, err = fmt.Sscanf(resp, "Offset: %d", &offset)
	if err != nil {
		return 0, fmt.Errorf("%q: %w", resp, err)
	}
	return offset, nil
}

// SetOffset sets the offset to the specified value
func (d *Device) SetOffset(offset int) error {
	if offset < MinOffset || offset > MaxOffset {
		return ErrOffset
	}
	_, err := d.Exec(fmt.Sprintf("f+%04d", offset))
	return err
}
