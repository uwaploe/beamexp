package beamexp

import (
	"bytes"
	"strings"
	"testing"
	"time"
)

type port struct {
	w    *strings.Builder
	r    *strings.Reader
	trig chan struct{}
}

func newPort(trigger chan struct{}) *port {
	p := port{
		w:    &strings.Builder{},
		trig: trigger,
	}
	return &p
}

func (p *port) setReader(s string) {
	p.r = strings.NewReader(s)
}

func (p *port) Read(b []byte) (int, error) {
	<-p.trig
	n, err := p.r.Read(b)
	if b[0] == '\n' {
		time.Sleep(time.Millisecond)
	}
	return n, err
}

func (p *port) Write(b []byte) (int, error) {
	return p.w.Write(b)
}

func TestScanner(t *testing.T) {
	table := []struct {
		in, out string
	}{
		{
			in:  "<- Offset: 0000\r\n-> ",
			out: "<- Offset: 0000\r\n",
		},
		{
			in:  "\x00\x00<- Offset: 0000\r\n-> ",
			out: "<- Offset: 0000\r\n",
		},
		{in: "-> ", out: ""},
	}

	var rbuf bytes.Buffer
	s := newScanner(&rbuf, PROMPT)
	for _, e := range table {
		rbuf.Write([]byte(e.in))
		resp, _ := s.next()
		if resp != e.out {
			t.Errorf("Mismatch; expected %q, got %q", e.out, resp)
		}
	}
}

func TestCommand(t *testing.T) {
	ch := make(chan struct{})
	p := newPort(ch)
	close(ch)

	table := []struct {
		in, out, resp string
	}{
		{
			in:   "?f",
			out:  "<- Offset: 0000\r\n-> ",
			resp: "Offset: 0000\r\n",
		},
	}

	d := New(p)
	var (
		resp string
		err  error
	)

	for _, e := range table {
		p.setReader(e.out)
		resp, err = d.Exec(e.in)
		if err != nil {
			t.Error(err)
		}
		if resp != e.resp {
			t.Errorf("Bad response; expected %q, got %q", e.resp, resp)
		}
	}
}

func TestOffset(t *testing.T) {
	ch := make(chan struct{})
	p := newPort(ch)
	close(ch)

	table := []struct {
		out   string
		value int
	}{
		{
			out:   "<- Offset: 0000\r\n-> ",
			value: 0,
		},
		{
			out:   "<- Offset: 0042\r\n-> ",
			value: 42,
		},
		{
			out:   "<- Offset: -0042\r\n-> ",
			value: -42,
		},
	}

	d := New(p)
	var (
		value int
		err   error
	)

	for _, e := range table {
		p.setReader(e.out)
		value, err = d.Offset()
		if err != nil {
			t.Error(err)
		}
		if value != e.value {
			t.Errorf("Bad response; expected %v, got %v", e.value, value)
		}
	}
}
